import Vue from 'vue'

import App from './App.vue';
import router from "./router";
import store from "./store";

import BootstrapVue from 'bootstrap-vue'
import { CarouselPlugin, ModalPlugin } from 'bootstrap-vue'
import { PopoverPlugin } from 'bootstrap-vue'
import Vuelidate from 'vuelidate'
import VueTheMask from 'vue-the-mask'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import LightBootstrap from './light-bootstrap-main'

Vue.use(PopoverPlugin)
Vue.use(VueTheMask)
Vue.use(Vuelidate)
Vue.use(CarouselPlugin)
Vue.use(BootstrapVue)
Vue.use(ModalPlugin)
Vue.use(LightBootstrap)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount("#app")
