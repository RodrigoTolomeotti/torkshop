import Vue from "vue";
import VueRouter from "vue-router";

import Home from '../views/Home.vue';
import Login from '../views/Login.vue';
import Usuario from '../views/Usuario.vue';
import Inicio from '../views/Inicio.vue';
import Produto from '../views/Produto.vue';
import DashboardLayout from '../layout/DashboardLayout.vue';
import Overview from '../views/Overview.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/login',
    component: Login,
    name: 'login',
    meta: {
      title: 'TorkShop - Login',
    }
  },
  {
    path: '/usuario',
    component: Usuario,
    children: [
      {
        name: "inicio",
        path: "inicio",
        alias: "/",
        component: Inicio,
        meta: {
          title: "TorkShop - Inicio"
        }
      }
    ]
  },
  {
  path: '/admin',
  component: DashboardLayout,
  redirect: '/admin/overview',
  children: [
    {
      path: 'overview',
      component: Overview,
      name: 'Overview'
    },
    {
      name: "produto",
      path: "produto",
      component: Produto,
      meta: {
        title: "TorkShop - Produtos"
      }
    },
  ]
},
{
  path: '/',
  component: Home,
  name: 'home',
  meta: {
    title: "TorkShop"
  }
}
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
