import APIService from "@/services/APIService.js";

const api = APIService.create({
  baseURL: process.env.VUE_APP_API_URL + 'usuario'
});

export default {
  authenticate(email, password) {
    return api.post("/login", {
      email: email,
      password: password
    });
  },
  getCurrentAdmin() {
    return api.get();
  }
};
