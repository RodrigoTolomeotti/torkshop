import APIService from "@/services/APIService.js";

const apiAdmin = APIService.create({
  baseURL: process.env.VUE_APP_API_URL + "/usuario"
});

export default {
  authenticate(email, password) {
    return apiAdmin.post("/login", {
      email: email,
      senha: password
    });
  },
  getCurrentAdmin() {
    return apiAdmin.get();
  }
};
