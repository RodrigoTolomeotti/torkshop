import axios from "axios";

export default {
  create(options) {
    options = Object.assign(
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      },
      options
    );

    const api = axios.create(options);
    api.interceptors.request.use(
      config => {
        let token = localStorage.getItem("apiToken");
        if (token) {
          config.headers["Authorization"] = `Bearer ${token}`;
        }
        return config;
      },
      error => {
        return Promise.reject(error);
      }
    );

    return api;
  }
};
