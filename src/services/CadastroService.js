import APIService from "@/services/APIService.js";

const apiCadastro = APIService.create({
  baseURL: process.env.VUE_APP_API_URL + "cadastro"
});

export default {
  get(usuario) {
    return apiCadastro.get("", usuario);
  },
  create(usuario) {
    return apiCadastro.post("", usuario);
  }
};
